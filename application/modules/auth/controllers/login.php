<?php
 
class Login extends CI_Controller
{
 
    function profile()
    {
        $data = konfigurasi('Profile');
        $data['get_all_userdata'] = $this->Auth_model->get_by_id($this->session->userdata('id'));
        $this->template->load('v_todo', $data);
    }
    
    function __construct(){
        parent::__construct();     
        $this->load->model('m_login');
 
    }
 
    function index(){
        $this->load->view('v_login');
    }
 
    function aksi_login(){
 
        $user_name = $this->input->post('username');
        $user_password = base64_encode($this->input->post('password'));
        $where = array(
            'user_name' => $user_name,
            'user_password' => $user_password
            );
        $cek = $this->m_login->cek_login("t_user",$where)->num_rows();
        if($cek > 0){
 
            $data_session = array(
                'nama' => $user_name,
                'status' => "login"
                );
 
            $this->session->set_userdata($data_session);
            // $pesan['message'] =    "Pendaftaran berhasil";
            redirect(base_url('todo/todoapp'));
 
        }else{
            echo "Username dan password salah !";
        }
    }
 
    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('index.php'));
    }

    
}