<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 
 class Register extends CI_Controller {
     
     function __construct(){
         parent::__construct();
         $this->load->library(array('form_validation'));
         $this->load->helper(array('url','form'));
         $this->load->model('m_register'); //call model
     }
 
     public function index() {
 
         $this->form_validation->set_rules('username', 'USERNAME','required');
         $this->form_validation->set_rules('fullname', 'FULLNAME','required');
         $this->form_validation->set_rules('password','PASSWORD','required');
         $this->form_validation->set_rules('email','EMAIL','required|valid_email');
         
         if($this->form_validation->run() == FALSE) {
             $this->load->view('v_register');
         }else{
 
             $data['user_name']   =    $this->input->post('username');
             $data['user_fullname'] =    $this->input->post('fullname');
             $data['user_email']  =    $this->input->post('email');
             $data['user_password'] =    base64_encode($this->input->post('password'));
            //  print_r ('post');
            //  exit();
             $this->m_register->register($data);
             
             $pesan['message'] =    "Pendaftaran berhasil";
             
            //  $this->load->view('todo/todoapp',$pesan);
             redirect(base_url("todo/todoapp"));
         }
     }
 }